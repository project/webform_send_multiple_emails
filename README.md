## Webform Send Multiple Emails

Extends the Webform module Email Handler to send individual emails
when multiple recipients are added to the email "to" field.

### Requirements
This module requires the Webform module:
https://www.drupal.org/project/webform

### Maintainers

George Anderson (geoanders)
https://www.drupal.org/u/geoanders
